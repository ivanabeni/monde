mon.de profile site REDESIGN

FRONTEND SETUP

using Bootstrap v4.0.0-beta compiled CSS (for grid only) in /assets/sass/_bootstrap-basic-grid.scss

CSS - JS - IMG compression, LIVERELOAD made with gulp.js

original style written in assets/sass

original js written in assets/lib

original images in  assets/precompress

//----- setting everything up for nice SEO score

1. using async javascript calls

2. using LD-JSON Schema org

3. using hCard microformat

4. compiled stylesheet under style.css

5. compiled .js under assets/js/monde.min.js

6. compressed images under assets/images
