//Set a Cookie
function set_cookie(c_name, value, exdays) {
    if (typeof (exdays) == 'undefined') {
        exdays = '360';
    }
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);

    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
};
//Get a Cookie
function get_cookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            return unescape(y);
        }
    }
    return false;
};
// the semi-colon before function invocation is a safety net against concatenated
// scripts and/or other plugins which may not be closed properly.
;(function ($, window, document, undefined) {
    "use strict";

    // Create the defaults once
    var pluginName = 'cookie',
        defaults = {
            displayedHtml: 'We use cookies to ensure that we give you the best experience on our website. If you continue, you agree with <strong>our cookie policy</strong>.',
            cssPrefix: pluginName + '_',
            closeButtonUrl:'https://www.marktplatz-mittelstand.de/infocenter/wp-content/themes/infocenter/images/close.svg'
         };

    // The actual plugin constructor
    function Plugin(element, options) {
        this.element = element;
        this.settings = $.extend(defaults, options);

        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    // Avoid Plugin.prototype conflicts
    $.extend(Plugin.prototype, {
        init: function () {
            var settings = this.settings;
            var me = this.element;

             var defaultCssObject = {
                width: "100%",
                display: "none",
                position: "relative",
                padding: '10px 0'
            };

            if (typeof settings.fontFamily !== "undefined") {
                defaultCssObject.fontFamily = settings.fontFamily;
            }

            if (typeof settings.fontSize !== "undefined") {
                defaultCssObject.fontSize = settings.fontSize;
            }

            if (typeof settings.fontSize !== "undefined") {
                defaultCssObject.fontSize = settings.fontSize;
            }

            // Style cookie div
            var footerElement = $('<div/>', {
                id: settings.cssPrefix + 'bar',
                class:'container',
                css: defaultCssObject,
            });

            var createOverlay = function () {
                var textElement = $('<div/>', {
                    html: settings.displayedHtml,
                    id: settings.cssPrefix + 'cookie-text',
                        css: {
                        paddingLeft: '15px',
                        paddingRight: '0'
                    }
                });

                var closeButtonCssObject = {
                    cursor: 'pointer',
                    position: 'absolute',
                    top: '0',
                    bottom: '0',
                    margin: 'auto',
                    right: '15px',
                    width: '18',
                    maxHeight: '2rem'
                };

                var closeButton = $('<img/>', {
                    id: settings.cssPrefix + "close",
                    src: settings.closeButtonUrl,
                    css: closeButtonCssObject
                }).on('click', function () {
                    hideOverlay();
                });


                footerElement.append(textElement);
                footerElement.append(closeButton);

                $(me).append(footerElement);

            };
            var showOverlay = function () {
                footerElement.fadeIn(500);

            };
            var hideOverlay = function () {
                $('.cookie-holder').fadeOut(500, function () {
                    $(this).remove();
                });
            };

            var createCookie = function () {
                set_cookie(settings.cssPrefix + "cookie", true);
            };

            var readCookie = function () {
                return get_cookie(settings.cssPrefix + "cookie");
            };


            if (settings.devMode || !readCookie()) {
                createCookie();
                createOverlay();
                showOverlay();
            }
        },
    });

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" +
                    pluginName, new Plugin(this, options));
            }
        });
    };

})(jQuery, window, document);