jQuery(document).ready(function($) {
    //--SKYSCRAPER ON THE RIGHT MAKE STICKY
    $("#skyscraper-oben-rechts").sticky({
        topSpacing: 15,
        stopper: ".stopper"
    });
    //--FOOTER CLICK TO TOP
    $('._up').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 700);
        return false;
    });
    //--FIRE COOKIE LAW
    $(function () {
        $('.cookie-holder').cookie({
            displayedHtml: 'Diese Webseite verwendet Cookies, um Ihnen mehr Benutzerfreundlichkeit bieten zu können. <a rel="nofollow" href="https://www.marktplatz-mittelstand.de/Datenschutz#cookies" target="_blank">Mehr Informationen →</a>',
            devMode:true,
        });
    });
});